FROM 580260895509.dkr.ecr.us-east-1.amazonaws.com/jenkins-slave-amazon-corretto-8

# JMETER
ENV JMETER_VERSION apache-jmeter-3.3
RUN curl --progress-bar -L -o ${JMETER_VERSION}.tgz https://archive.apache.org/dist/jmeter/binaries/${JMETER_VERSION}.tgz && \
    tar -xvf ./${JMETER_VERSION}.tgz -C /usr/local && \
    chmod +x /usr/local/${JMETER_VERSION}/bin/jmeter && \ 
    export PATH=/usr/local/${JMETER_VERSION}/bin:$PATH && \
    jmeter --version